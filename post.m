clear;
close all;
x = load ( 'x.txt' );
[X,Y] = meshgrid(x,x);
t = load ( 't_f.txt' );
u = load ( 'u_f.txt' );

tn = length(t);
U = zeros(size(X));
dof = length(u(1,:));
chunksize = sqrt(dof);
up = zeros(chunksize,chunksize);

% for i = 1:chunksize
%     for j = 1:chunksize
%         up(i,j) = u_current(j+(i-1)*chunksize);
%     end
% end

for i = 1:tn
%     u_current = ;
    U(2:end-1,2:end-1) =reshape(u(i,:),chunksize,chunksize);
    mesh(X,Y,U);
    set(gca,'zlim',[0 20]);
    pause(0.1);
end

