#include "utility.h"
void dtable_data_write ( ofstream &output, int m, int n, float table[], bool post , int jump )
//  Parameters:
//
//    Input, ofstream &OUTPUT, a pointer to the output stream.
//
//    Input, int M, the spatial dimension.
//
//    Input, int N, the number of points.
//
//    Input, float TABLE[M*N], the table data.
//
//    Input, bool post, flag to output fine solution
//    Input, int jump, which time marked solution is not output
{
  int i;
  int j;
 if(post){
  for ( j = 0; j < n; j++ )
  {
    if(j%(jump+1) == jump && j!=(n-1))
      continue;
    else{
	for ( i = 0; i < m; i++ )
	{
	  output << setw(10) << table[i+j*m] << "  ";
	}
	output << "\n";
    }
  }
 }
 else{
  for ( j = 0; j < n; j++ )
  {
	for ( i = 0; i < m; i++ )
	{
	  output << setw(10) << table[i+j*m] << "  ";
	}
	output << "\n";
  }
 }
  return;
}

void dtable_write ( string output_filename, int m, int n, float table[], bool header,bool post,int n_fine )
//
//  Purpose:
//
//    DTABLE_WRITE writes information to a DTABLE file.
//
//
//  Parameters:
//
//    Input, string OUTPUT_FILENAME, the output filename.
//
//    Input, int M, the spatial dimension.
//
//    Input, int N, the number of points.
//
//    Input, float TABLE[M*N], the table data.
//
//    Input, bool HEADER, is TRUE if the header is to be included.
//
//    Input, bool post, flag to output fine solution
//    Input, int n_fine, which time marked solution is not output
{
  int i,id;
  ofstream output;

  output.open ( output_filename.c_str ( ) );

  if ( !output )
  {
    cerr << "\n";
    cerr << "DTABLE_WRITE - Fatal error!\n";
    cerr << "  Could not open the output file.\n";
    return;
  }

  if ( header )
  {
//  dtable_header_write ( output_filename, output, m, n );
  }
  if(!post)
	    dtable_data_write ( output, m, n, table );
  else{ 
            dtable_data_write(output, m, n, table,post,n_fine);
  }
  
  output.close ( );

  return;
}


void timestamp ( )
{
# define TIME_SIZE 40

  static char time_buffer[TIME_SIZE];
  const struct tm *tm;
  size_t len;
  time_t now;

  now = time ( NULL );
  tm = localtime ( &now );

  len = strftime ( time_buffer, TIME_SIZE, "%d %B %Y %I:%M:%S %p", tm );

  cout << time_buffer << "\n";

  return;
# undef TIME_SIZE
}

void f ( float a, float b, float t0, float t, int n, float x[], 
  float value[] )


//  Purpose:
//
//    F returns the right hand side of the heat equation.
//
//
//  Parameters:
//
//    Input, float A, B, the left and right endpoints.
//
//    Input, float T0, the initial time.
//
//    Input, float T, the current time.
//
//    Input, int N, the number of points.
//
//    Input, float X[N], the current spatial positions.
//
//    Output, float VALUE[N], the prescribed value of U(X(:),T0).
//
{
  int i;

  for ( i = 0; i < n; i++ )
  {
    value[i] = 0.0;
  }
  return;
}


void u0 ( float a, float b, float t0, int n, float x[], float value[] )
//  usage: u0 ( x_min, x_max, t_min, x_num, x, u_F);
//  Purpose:
//
//    U0 returns the initial condition at the starting time.
//
//  Parameters:
//
//    Input, float A, B, the left and right endpoints
//
//    Input, float T0, the initial time.
//
//    Input, float T, the current time.
//
//    Input, int N, the number of points where initial data is needed.
//
//    Input, float X[N], the positions where initial data is needed.
//
//    Output, float VALUE[N], the prescribed value of U(X,T0).
//
{
  int i;

  for ( i = 0; i < n; i++ )
  {
    //value[i] = 100.0;
    value[i] = x[i]*(0.3-x[i])*10+20;
//     value[i] = 100;
  }
  return;
}

void u0_2d(float a, float b, float t0, int chunkSize, float x[], float y[],float u[]){

  int i,j,row,col;
  
  float *x_corr = x+1;
  float *y_corr = y+1;
  float mid = 0.5*(a+b);
  float trd = 2.f/3.f*(a+b);
  float left_b = mid-((b-a)/2.f)*0.2;
  float right_b = mid+((b-a)/2.f)*0.2;
  float left_a = trd-((b-a)/2.f)*0.2;
  float right_a = trd+((b-a)/2.f)*0.2;
  #pragma omp parallel for private(i,j,col,row)
  for ( i = 0; i < chunkSize; i++){
    for (j = 0; j < chunkSize;j++){
        if ((left_a<=y_corr[j])&&(y[j]<=right_a)&&(left_a<=x_corr[i]&&(x_corr[i]<=right_a)))
            u[i*chunkSize+j]=20.f;
        else if ((left_b<=y_corr[j])&&(y[j]<=right_b)&&(left_b<=x_corr[i]&&(x_corr[i]<=right_b)))
            u[i*chunkSize+j]=15.7f;
        else if ((left_b*0.5<=y_corr[j])&&(y[j]<=right_b*0.5)&&(left_a*0.3<=x_corr[i]&&(x_corr[i]<=right_a*0.3)))
            u[i*chunkSize+j]=20.f;
        else
            u[i*chunkSize+j]=0.f;
    }
  }
  return;
}

float ua ( float a, float b, float t0, float t )
//
//  Purpose:
//
//    UA returns the Dirichlet boundary condition at the left endpoint.
//
//  Parameters:
//
//    Input, float A, B, the left and right endpoints
//
//    Input, float T0, the initial time.
//
//    Input, float T, the current time.
//
//    Output, float UA, the prescribed value of U(A,T).
//
{
  float value;

  value = 20.0;

  return value;
}
float ub ( float a, float b, float t0, float t )
//  Purpose:
//
//    UB returns the Dirichlet boundary condition at the right endpoint.
//  Parameters:
//
//    Input, float A, B, the left and right endpoints
//
//    Input, float T0, the initial time.
//
//    Input, float T, the current time.
//
//    Output, float UB, the prescribed value of U(B,T).
//
{
  float value;

  value = 20.0;

  return value;
}

void setMatrix(float **a, int chunkSize, float w)
{
  int i,j,k;
  int dofA = chunkSize*chunkSize;

  
  int ori_x,ori_y;
  #pragma omp parallel for private(ori_x,ori_y)
  for ( k = 0;k < chunkSize;k++){
    ori_x = k*chunkSize;
    ori_y = ori_x;
    for (int i = 1;i<chunkSize-1;i++){
        a[ori_x+i][ori_y+i] = 1.0f+4.f*w;
        a[ori_x+i][ori_y+i-1] = -w;
        a[ori_x+i][ori_y+i+1] = -w;
    }
    a[ori_x][ori_y] = 1.0f+4.f*w;
    a[ori_x][ori_y+1] = -w;
    a[ori_x+chunkSize-1][ori_y+chunkSize-1] = 1.0f+4.f*w;
    a[ori_x+chunkSize-1][ori_y+chunkSize-1-1] = -w;
    }
  

#pragma omp parallel for private(ori_x,ori_y)
  for ( k = 0;k < chunkSize-1;k++){
    ori_x = k*chunkSize;
    ori_y = ori_x + chunkSize;
    for (int i = 0;i<chunkSize;i++){
      // for(int j = 0; j<chunkSize;j++){
        a[ori_x+i][ori_y+i] = -w;
        a[ori_y+i][ori_x+i] = -w;
        // a[ori_x+i+chunkSize][ori_y+j-chunkSize] = -w;
      // }
    }
  }

  // a[0][0] = 1.0f + 4.0f*w;
  // a[0][1] = -w;
  // a[dofA-1][dofA-1] = 1.0f + 4.0f*w;
  // a[dofA-1][dofA-2] = -w;

  // for ( i = 1; i < dofA-1; i++ ){
  //   a[i][i] = 1.0f+4.0f*w;
  //   a[i][i+1] = -w;
  //   a[i][i-1] = -w;
  //   // cout<<w<<endl;
  //   // cout<<1.f+4.f*w<<endl;
  // }
  
  // for (i = 0 ;i < chunkSize; i++){
  //   a[i][i+chunkSize] = -w;
  // }

  // for (i = dofA-chunkSize ;i < dofA; i++){
  //   a[i][i-chunkSize] = -w;
  // }

  // for (i = chunkSize; i < dofA - chunkSize;i++){
  //   a[i][i+chunkSize] = -w;
  //   a[i][i-chunkSize] = -w;
  // }
}

void printMat(int dim,float **mat){
    cout << "You have entered the matrix :- " << endl;
    int i,j;
    for ( i = 0; i < dim; i++ ) {
      for ( j = 0; j < dim; j++ ) {
         printf("%10.6f ",mat[i][j]);
      }
      printf("\n");
   }

}
void printVtr(int dim,float *vtr){
    int i;
    for (int i = 0; i < dim ; i++ )
    {
      printf("%10.6f ",vtr[i]);
    }
    printf("\n");
}
void printVtr(int dim,int *vtr){
    int i;
    for (int i = 0; i < dim ; i++ )
    {
      printf("%d ",vtr[i]);
    }
    printf("\n");
}
void RBordering(int chunk,int dof,unsigned int **redIdx,int *rl,unsigned int **blkIdx,int *bl){
    // red -black implemantation
   unsigned int redLast = (0==chunk%2)?(chunk-2):(chunk-1);//in line zero
   unsigned int red1DLen = redLast/2+1;
   unsigned int red2DLen = (red1DLen)*(red1DLen)+(chunk-red1DLen)*(chunk-red1DLen);
   unsigned int blk2DLen = dof - red2DLen;

   *redIdx = (unsigned int*)malloc(sizeof(unsigned int)*red2DLen);
   *blkIdx = (unsigned int*)malloc(sizeof(unsigned int)*blk2DLen);
   
   

   // openmp acc
   unsigned int cntr = 0;
   unsigned int cntb = 0;
   for (int i = 0; i < chunk; i++){
    for (int l = 0; l < chunk; l++){
        if((i+l)%2 == 0){
          (*redIdx)[cntr] = i*chunk+l;
          cntr ++ ;
        }
        else{
          (*blkIdx)[cntb] = i*chunk+l;
          cntb ++ ;
        }
   }
  }
  *rl = red2DLen;
  *bl = blk2DLen;
  //
}