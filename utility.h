# include <cstdlib>
# include <iostream>
# include <iomanip>
# include <fstream>
# include <ctime>
# include <cmath>
# include <string.h>
using namespace std;
void timestamp ( );
void dtable_data_write ( ofstream &output, int m, int n, float table[] ,bool post = false,int jump = 0);
void dtable_write ( string output_filename, int m, int n, float table[], bool header,bool post=false,int jump=0 );

void heat2dBe_GS(float **A,int redLen,unsigned int redIdx[],int blkLen, unsigned int blkIdx[], int n_step,int dof,int chunk, float u[]);
void heat2dBe_CG(float val[],int col_ind[],int row_ptr[],int n_step,int dof, float u[]);
void f ( float a, float b, float t0, float t, int n, float x[], float value[] );
void u0 ( float a, float b, float t0, int n, float x[], float value[] );
void u0_2d(float a, float b, float t0, int chunkSize, float x[], float y[],float u[]);
float ua ( float a, float b, float t0, float t );
float ub ( float a, float b, float t0, float t );
void MapC2F(int n_Coarse, int x_num, float u_C[],float u_F[],int pick[]);
void MapC2corr(int n_Coarse, int x_num, float u_C[],float uC_corr[]);
void UpdateCoarse(int len,float a[],float b[],float c[]);
void setMatrix(float **a,int chunkSize,float w);
void printMat(int dim,float **mat);
void printVtr(int dim,float *vtr);
void printVtr(int dim,int *vtr);
void RBordering(int chunk,int dof,unsigned int **redIdx,int *rl,unsigned int **blkIdx,int *bl);