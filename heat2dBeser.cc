#include "utility.h"
#include "GS_solver.h"
#include "CG_solver.h"
# include <x86intrin.h>
# include <xmmintrin.h>
# include <smmintrin.h>
#include <omp.h>
#include <mpi.h>
#define BYTE_NUMBER 32
#define DATA_NUMBER 4
extern int MaxIter;

void heat2dBe_GS(float **A, int redLen,unsigned int redIdx[],int blkLen, unsigned int blkIdx[], int n_step,int dof,int chunk, float u[])
{
// zero bc, zero f
  int numit;//record how many iters used
  float epsilon = 0.000001f;
  float *b = new float[dof];
  float *fvec = new float[dof];

  for ( int j = 1; j <n_step; j++ )
  {
//
//  Set the right hand side B.
//
    // b[0] = 20;// in 2d not necessary
    // b[x_num-1] = 20;

    memcpy(b,u+(j-1)*dof,sizeof(float)*dof);

    // run_gauss_seidel_method ( dof, A, b,epsilon, MaxIter,&numit, fvec );
    run_gauss_seidel_RB_method ( dof, chunk, redLen,  redIdx, blkLen,  blkIdx,A, b,epsilon, MaxIter,&numit, fvec  );
    if (numit >= MaxIter)
        cout<<"Converge in "<<numit<<" Iters"<<endl;
    
    memcpy(u+j*dof,fvec,sizeof(float)*dof);
  }
  delete [] b;
  delete [] fvec;
}

void MapC2F(int n_Coarse, int dof, float u_C[],float u_F[],int pick[])
{
  for (int i=0;i<n_Coarse;i++){
    int id = pick[i];
    memcpy(u_F+dof*id,u_C+dof*i,sizeof(float)*dof);
  }
}

void MapC2F_MPI(int dof,int n_Fine,int numMyProc, float sub_u_C[],float u_F[])
{
  int offset,  offset1 ;
  for(int i = 0;i < numMyProc; i++){
    offset = (n_Fine+1)*i*dof;
    offset1 = i*dof;
    memcpy(u_F+offset,sub_u_C+offset1,sizeof(float)*dof);
  //   for(int j=0;j<dof;j++){
  //       u_F[offset+j] = sub_u_C[offset1+j];
  // }
  }
}

void MapC2corr(int n_Coarse, int dof, float u_C[],float uC_corr[])
{
  for (int i=0;i<n_Coarse;i++){
    memcpy(uC_corr+dof*2*i,u_C+dof*i,sizeof(float)*dof);
  }
}

void MapC2corr_MPI(int numMyProc, int dof, float u_C[],float uC_corr[])
{
  for (int i=0;i<numMyProc;i++){
    int id = 2*i;
    memcpy(uC_corr+dof*id,u_C+dof*i,sizeof(float)*dof);
  //   for (int j=0;j<dof;j++){
  //        uC_corr[j+dof*id] = u_C[j+dof*i];
  // }
  }
}


void fillbuf(float *sendbuf,float *u_F,int numMyProc,int dof,int n_Fine)
{
  int offset=0;
  int offset1 = n_Fine*dof;
  for (int i=0;i<numMyProc;i++){
    offset = i*dof;
    offset1 = n_Fine*dof+ i*dof*(n_Fine+1);

    memcpy(sendbuf+offset,u_F+offset1,sizeof(float)*dof);

    // for(int j = 0;j<dof;j++){
    //   sendbuf[j+offset]  = u_F[j+offset1];
    // }
  }
}

void UpdateCoarse(int len,float a[],float b[],float c[])//a+=b-c
{
    for (int i=0;i<len;i++){
      a[i]+=(b[i]-c[i]);
  }
}



void heat2dBe_CG(float val[],int col_ind[],int row_ptr[],int n_step,int dof, float u[])
{
// zero bc, zero f
  int numit;//record how many iters used
  float epsilon = 0.000001f;
  float *b = (float*)aligned_alloc(BYTE_NUMBER,sizeof(float)*dof);
  float *fvec = (float*)aligned_alloc(BYTE_NUMBER,sizeof(float)*dof);

  for ( int j = 1; j <n_step; j++ )
  {
//
//  Set the right hand side B.
//
    // b[0] = 20;// in 2d not necessary
    // b[x_num-1] = 20;

    memcpy(b,u+(j-1)*dof,sizeof(float)*dof);

    numit = cg_solve(dof, val,col_ind,row_ptr,b,epsilon,MaxIter,fvec);
    
    if (numit >= MaxIter)
        cout<<"Converge in "<<numit<<" Iters"<<endl;
    
  
    memcpy(u+j*dof,fvec,sizeof(float)*dof);

  }
  free(b);
  free(fvec);
}
