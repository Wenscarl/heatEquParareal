BIN_SUFF =
CXX = g++ 
CXXWARN = -Wno-deprecated
OPTFLAGS = -O3 -mtune=generic -march=native -ftree-vectorizer-verbose=2 -fopenmp #-fopt-info
#OPTFLAGS = -O3 -march=native 
GDBFLAGS = -g
CFLAGS = -msse4.1  -fopenmp 

SRC_CXX = main.cc utility.cc heat2dBe.cc GS_solver.cc CG_solver.cc 
objects = $(SRC_CXX:.cc=.o)

all: gdb opt 
opt: 
gdb: BIN_SUFF = _gdb
BIN = pararealsolver$(BIN_SUFF)

opt: $(objects) Makefile
	$(CXX) $(CFLAGS) -o $(BIN) $(objects) $(CXXWARN) $(OPTFLAGS) 
gdb: $(objects) Makefile
	$(CXX) $(CFLAGS) -o $(BIN) $(objects) $(CXXWARN) $(GDBFLAGS)  
$(objects): %.o: %.cc Makefile
	$(CXX) $(CFLAGS) -c $< -o $@ $(OPTFLAGS) $(CXXWARN)

.PHONY : clean
clean :
	find . -type f | xargs -n 5 touch
	rm -f default $(objects) $(BIN)* *.~ *.txt *.o
