# include <cstdlib>
# include <iostream>
# include <iomanip>
# include <fstream>
# include <ctime>
# include <cmath>
# include <string>
# include <cstring>
# include <xmmintrin.h>
# include <smmintrin.h>
# include <omp.h>
typedef __m128 FloatIntrinsic;
void run_gauss_seidel_method ( int n, float **A, float *b,float epsilon, int maxit,int *numit, float *x );
void run_gauss_seidel_RB_method ( int n, int chunk, int redLen, unsigned int redIdx[], int blkLen, unsigned int blkIdx[], float **A, float *b,  float epsilon, int maxit,   int *numit, float *x );
