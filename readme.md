This is the MPI version running of ULAM.
1) type make the compile.
2) mpirun -np N ./pararealsolver 12 12 6 66 600 1 1 > message to run the code on N processes. To fine configure the locations of these nodes, please refer run.pbs for detail, especially #PBS -lnodes=N:ppn=M, M is less and equal than 8.
3) To view the result, run MATLAB script postparllel.m under the same folder. 
