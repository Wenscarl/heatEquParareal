#ifndef __CG__
#define __CG__
#include <math.h>
#include <stdio.h>
#include <omp.h>

void spmxv(const float* val,const int *col,const int *row, const float *x,float *v,int nrows);
void A_to_crs( float** A,float **A_fine,int chunkSize,int dofA, float **val,float **val_fine,int **col_ind,int **row_ptr);
void axpy(float *dest, float a, float *x, float *y, int n);
float innerProd(float *x, float *y, int n);
void vtrEqual(float a[],float b[],int n);
int cg_solve(int dofA, float val[],int col_ind[],int row_ptr[],float b[],float epsilon,int maxit,float *x);
#endif