# include <cstdlib>
#include <stdio.h>
#include <sys/time.h>
# include <iostream>
# include <iomanip>
# include <fstream>
# include <ctime>
# include <cmath>
# include <x86intrin.h>
# include <xmmintrin.h>
# include <smmintrin.h>
# include <stdlib.h>
# include "utility.h"
# include "GS_solver.h"
# include "CG_solver.h"
#include <omp.h>
#define BYTE_NUMBER 32
#define MICROSEC_IN_SEC 1000000
using namespace std;
int MaxIter ;
int main (int argc,char *argv[] )
//
// All changes to code are copyright, 2017, shu wang, shuwang12@unm.edu
//  Purpose:
//
//   This is the main program for solving heat equation by parareal algorithm.
//
//  Note:
//
//    Parareal solves the heat equation with an implicit method, backward euler.
//
//    This program solves
//
//      dUdT - k * d2UdX2 = F(X,T)
//
//    over the interval [A,B] with boundary conditions
//
//      U(A,T) = UA(T),
//      U(B,T) = UB(T),
//
//    over the time interval [T0,T1] with initial conditions
//
//      U(X,T0) = U0(X)
//
//
//  Licensing: 
//  UNM CS 542 
//
//
//  Modified:
//
//    07 Feb 2017
//
//  Author:
//
//    Shu Wang
//
{
  float **a,**a_fine;
  bool header;
  int i;
  int j;
  int n_Coarse,n_FineTot,n_Fine;
  int IterK ;
  float k;
  float *t_Coarse,*t_Fine;
  float t_dtFine;
  float t_dtCoarse;
  string t_file;
  float t_max;
  float t_min;
  float *u_C,*u_F,*uC_corr;
  string u_file;
  float w_coarse,w_fine;
  float *x,*y;
  float x_delt;
  string x_file;
  float x_max;
  float x_min;
  int x_num;
  int solverType;
  int numTHREAD;
  if(argc >=7){
    n_Coarse = atoi(argv[1]);
    n_Fine = atoi(argv[2]);
    IterK = atoi(argv[3]);
    x_num = atoi(argv[4]);
    MaxIter = atoi(argv[5]);
    solverType = atoi(argv[6]);
    numTHREAD = atoi(argv[7]);
  }
  else{//default
    n_Coarse = 5;
    n_Fine = 4;
    IterK =  5;
    x_num = 45;
    MaxIter = 700;
    solverType = 1;//default Conjugate Gradient
    numTHREAD = 1;
  }
  n_FineTot = n_Coarse*n_Fine;
  timestamp ( );
  printf("\n");
  printf("FD2D_HEAT_IMPLICIT_BY_ParaReal\n");
  printf("  C++ version\n");
  printf( "\n");
  printf("  Finite difference solution of\n");
  printf( "  the time dependent 1D heat equation\n");
  printf("\n");
   printf("    Ut - k * Uxx = F(x,t)\n");
   printf( "\n");
   printf("  for space interval A <= X <= B with boundary conditions\n");
   printf( "\n");
   printf("    U(A,t) = UA(t)\n");
   printf("    U(B,t) = UB(t)\n");
   printf("\n");
   printf( "  and time interval T0 <= T <= T1 with initial condition\n");
   printf( "\n");
  printf("    U(X,T0) = U0(X).\n");
  printf("\n");
   printf("  A second order difference approximation is used for Uxx.\n");
  printf("\n");
   printf("  A first order backward Euler difference approximation\n");
  printf("  is used for Ut.\n");
  if (solverType==1)
  printf( " Conjugate Gradient iterative solver is used!\n");
  else
   printf( " Gauss-Seidel iterative solver is used!\n");
  printf( " Coarse Grids = %d\n",n_Coarse);
   printf( " Fine Grids = %d\n",n_FineTot);
    printf( " Iterations = %d\n",IterK);

  printf("  is used for Ut.\n");
  
  
  k = 5.0E-07;
//
//  Set X values.
//
  x_min = 0.0;
  x_max = 0.3;
//   x_num = 11;
  x_delt = ( x_max - x_min ) / ( float ) ( x_num - 1 );
  x = (float*)malloc(sizeof(float)*x_num);
  y = (float*)malloc(sizeof(float)*x_num);
  for ( i = 0; i < x_num; i++ )
  {
    x[i] = ( ( float ) ( x_num - i - 1 ) * x_min  + ( float ) (i) * x_max )  / ( float ) ( x_num - 1 );
    y[i] = ( ( float ) ( x_num - i - 1 ) * x_min  + ( float ) (i) * x_max )  / ( float ) ( x_num - 1 );
  }
// 
//  Set T values.
//
  t_min = 0.0;
  t_max = 1000.0;
  
  t_dtCoarse = ( t_max - t_min ) / ( float ) ( n_Coarse );
  t_dtFine = ( t_max - t_min ) / ( float ) ( n_FineTot );
  
  t_Coarse = (float*)malloc(sizeof(float)*(n_Coarse+1));
  t_Fine = (float*)malloc(sizeof(float)*(n_FineTot+1));
  
    for ( j = 0; j < n_Coarse+1; j++ ){
    t_Coarse[j] = ( ( float ) ( n_Coarse+1 - j - 1 ) * t_min   
           + ( float ) (         j     ) * t_max ) 
           / ( float ) ( n_Coarse+1     - 1 );
  }
    for ( j = 0; j < n_FineTot+1; j++ ){
    t_Fine[j] = ( ( float ) ( n_FineTot+1 - j - 1 ) * t_min   
           + ( float ) (         j     ) * t_max ) 
           / ( float ) ( n_FineTot+1     - 1 );
  }
  
  int *pick = (int*)malloc(sizeof(int)*(n_Coarse+1));
  for (j=0;j<n_Coarse+1;j++){
    pick[j] = j*(n_Fine+1);
  }
//
//  The matrix A does not change with time.  We can set it once,
//  factor it once, and solve repeatedly.
//
  int chunkSize = x_num-2;
  int dofA = chunkSize*chunkSize;
  w_coarse = k * t_dtCoarse / x_delt / x_delt;
  
  a = (float**)malloc(sizeof(float*)*dofA);
  for (int i = 0;i < dofA;i++){
    a[i] = (float*)malloc(sizeof(float)*dofA);
    memset(a[i],0.0,dofA*sizeof(float));
  }
 // set matrix for coarse grid
  setMatrix(a,chunkSize,w_coarse);
    // printMat(dofA,a);
  w_fine = k * t_dtFine / x_delt / x_delt;
  a_fine = (float**)malloc(sizeof(float*)*dofA);
  for (int i = 0;i < dofA;i++){
    a_fine[i] = (float*)malloc(sizeof(float)*dofA);
    memset(a_fine[i],0.0,dofA*sizeof(float));
  }
  setMatrix(a_fine,chunkSize,w_fine);
   // printMat(dofA,a_fine);
  // setup red-black ordering
  unsigned int *redIdx,*blkIdx;
  int redLen,blkLen;
  RBordering( chunkSize, dofA,&redIdx,&redLen,&blkIdx,&blkLen);
  // Start-up serial;  Set the initial data, for time T_MIN.
  
  u_C = (float*)aligned_alloc(BYTE_NUMBER,sizeof(float)*(dofA*(n_Coarse+1)));
  u_F = (float*)aligned_alloc(BYTE_NUMBER,sizeof(float)*(dofA*(n_Coarse*(n_Fine+1))));
  uC_corr =(float*)aligned_alloc(BYTE_NUMBER,sizeof(float)*(dofA*(n_Coarse*2)));

  // setting up initial condition 1d
  // u0 ( x_min, x_max, t_min, x_num, x, u_F);
  // u0 ( x_min, x_max, t_min, x_num, x, u_C);
  // u0 ( x_min, x_max, t_min, x_num, x, uC_corr);
  // setting up initial condition 2d, NOTICE: initialize first data of u
  u0_2d(x_min, x_max, t_min, chunkSize, x, y, u_F);
  u0_2d(x_min, x_max, t_min, chunkSize, x, y, u_C);
  u0_2d(x_min, x_max, t_min, chunkSize, x, y, uC_corr);
   //heat1dBe_GS(n_Coarse+1,x_num,a_cr_coarse,u_C);
  //
  ////

  float *val,*val_fine;
  int *col_ind,*row_ptr;
  if(solverType == 1)  A_to_crs(a,a_fine,chunkSize,dofA, &val,&val_fine,&col_ind,&row_ptr);
  ////


  //******** Parareal Loop *********//
  omp_set_num_threads(numTHREAD);
  struct timeval tv,tv_serial;
  gettimeofday(&tv,NULL);
  long start = tv.tv_usec + tv.tv_sec * MICROSEC_IN_SEC;
  long serial_time = 0;
  for (int k = 0;k < IterK; k++){
      printf("Iteration %d\n",k);
      #pragma omp parallel 
      {
         #pragma omp single
         {
                printf("NUM OF OPENMP THREADS = %d\n", omp_get_num_threads() );
         }
          // MapC2F(n_Coarse, dofA, u_C,u_F,pick);
          #pragma omp for schedule(dynamic) 
         for (int i=0;i<n_Coarse;i++) memcpy(u_F+dofA*pick[i],u_C+dofA*i,sizeof(float)*dofA); 

          #pragma omp for schedule(dynamic) nowait
          for(int j = k;j<n_Coarse;j++){
              float *sliceBegin = u_F+dofA*pick[j];
              // 
              if(solverType == 1)
                heat2dBe_CG(val_fine,col_ind,row_ptr,n_Fine+1,dofA,sliceBegin);
              else
               heat2dBe_GS(a_fine,redLen,redIdx,blkLen, blkIdx,n_Fine+1,dofA,chunkSize,sliceBegin);               
          } 
          // correction-parallel
          // MapC2corr(n_Coarse,dofA, u_C, uC_corr);
          #pragma omp for schedule(dynamic) 
          for (int i=0;i<n_Coarse;i++)    memcpy(uC_corr+dofA*2*i,u_C+dofA*i,sizeof(float)*dofA);
        
          #pragma omp for schedule(dynamic)
            for (int j=k;j<n_Coarse;j++){
            float *sliceBegin = uC_corr+j*dofA*(1+1);

            if(solverType == 1)
            heat2dBe_CG(val,col_ind,row_ptr,2,dofA,sliceBegin);
            else
              heat2dBe_GS(a, redLen,redIdx,blkLen,blkIdx,2,dofA,chunkSize,sliceBegin);
          }
                
          // update-serial
            #pragma omp single
            {
                gettimeofday(&tv_serial,NULL);
                long start_serial = tv_serial.tv_usec + tv_serial.tv_sec * MICROSEC_IN_SEC;
                  for (int j=k;j<n_Coarse;j++){
                      float *sliceBegin = u_C+j*dofA;
                      if(solverType == 1)
                       heat2dBe_CG(val,col_ind,row_ptr,2,dofA,sliceBegin);
                    else
                      heat2dBe_GS(a, redLen,redIdx,blkLen,blkIdx,2,dofA,chunkSize,sliceBegin);

                      float *pC = u_C+(j+1)*dofA;
                      float *pF = u_F+(pick[j+1]-1)*dofA;
                      float *p_corr = uC_corr+(j*2+1)*dofA;
                      UpdateCoarse(dofA,pC,pF,p_corr);
                  }
                gettimeofday(&tv_serial,NULL);
                serial_time+=(tv_serial.tv_sec * MICROSEC_IN_SEC + tv_serial.tv_usec - start_serial) / 1000;
          }
    }
    
      }

  gettimeofday(&tv, NULL);
  long diff = (tv.tv_sec * MICROSEC_IN_SEC + tv.tv_usec - start) / 1000;
  printf("Parareal Time taken: %ld seconds %ld milliseconds\n", diff / 1000, diff % 1000);
  printf("Parallel part Time taken: %ld seconds %ld milliseconds\n", (diff-serial_time) / 1000, (diff-serial_time) % 1000);
  
  x_file = "x.txt";
  header = false;
  dtable_write ( x_file, 1, x_num, x, header );
  cout << "\n";
  cout << "  X data written to \"" << x_file << "\".\n";
  t_file = "t_c.txt";
  header = false;
  dtable_write ( t_file, 1, n_Coarse+1, t_Coarse, header );
  cout << "  T coarse data written to \"" << t_file << "\".\n";
  
  t_file = "t_f.txt";
  dtable_write ( t_file, 1, n_FineTot+1, t_Fine, header );
  cout << "  T fine data written to \"" << t_file << "\".\n";
  
  u_file = "u_c.txt";
  header = false;
  dtable_write ( u_file, dofA, n_Coarse+1, u_C, header);
  cout << "  U coarse data written to \"" << u_file << "\".\n";
  
  u_file = "u_f.txt";
  header = false;
  dtable_write ( u_file, dofA, n_Coarse*(n_Fine+1), u_F, header ,true,n_Fine);
  cout << "  U fine data written to \"" << u_file << "\".\n";
  
  cout << "\n";
  cout << "FD2D_HEAT_IMPLICIT\n";
  cout << "  Normal end of execution.\n";
  cout << "\n";
  timestamp ( );
// free memory
  for (int i = 0;i < dofA; i++ )  {free(a[i]);free(a_fine[i]);}
  free(a);
  free(a_fine);
  free(t_Coarse);
  free(t_Fine);
  free(pick);
  free(u_C);
  free(u_F);
  free( uC_corr);
  free(x);
  free(y);
  free(redIdx);
  free(blkIdx);
  
  if(solverType == 1){
  free(val);
  free(val_fine);
  free(col_ind);
  free(row_ptr);
  }
  return 0;
}