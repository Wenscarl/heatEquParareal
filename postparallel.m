clear;
close all;
info = load('info.txt');
num_proc = info(1);
n_coarse = info(2);
n_fine = info(3);
numMyproc = info(4:end);

x = load ( 'x.txt' );
[X,Y] = meshgrid(x,x);
t = load ( 't_c.txt' );
t_f = load ( 't_f.txt' );
u_c = load ( 'u_c.txt' );

tn = length(t);
tn_f = length(t_f);
U = zeros(size(X));
dof = length(u_c(1,:));
chunksize = sqrt(dof);
up = zeros(chunksize,chunksize);

% for i = 1:chunksize
%     for j = 1:chunksize
%         up(i,j) = u_current(j+(i-1)*chunksize);
%     end
% end

subplot(121)

for i = 1:tn
%     u_current = ;
    U(2:end-1,2:end-1) =reshape(u_c(i,:),chunksize,chunksize);
    mesh(X,Y,U);
    set(gca,'zlim',[0 20]);
    pause(0.1);
end
dt = t(end)/tn;
title(['dt = ',num2str(dt),' ',num2str(tn),' steps']);

U_F=[];
for i = 1:num_proc
    fname = sprintf('u_f_%d.txt',i-1);
    u_f = load(fname);
    regions = numMyproc(i);
    for j=1:regions
        tmp = u_f((1:n_fine)+(j-1)*(1+n_fine),:);
        U_F = [U_F;tmp];
    end
        
    if(i==num_proc)
       U_F = [U_F;u_f(end,:)];
    end
end

subplot(122)

for i = 1:tn_f
%     u_current = ;
    U(2:end-1,2:end-1) =reshape(U_F(i,:),chunksize,chunksize);
    mesh(X,Y,U);
    set(gca,'zlim',[0 20]);
    pause(0.1);
end
dt = t_f(end)/tn_f;
title(['dt =',num2str(dt),' ',num2str(tn_f),' steps']);
