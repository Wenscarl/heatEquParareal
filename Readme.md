This is the openMP version. 
To compile the code, just type 'make' in command line. 
run:
./pararealsolver n_coarse n_fine(per coarse grid) iterK x_num maxIter solverType

solverType: 0 Gauss-Seidel ;1 Conjugate Gradient
We always choose the more efficient solver, Conjugate Gradient, and all the analysis and testing are based on him.

example: ./pararealsolver 12 12 6 66 600 1 1
The parameters are:
n_Coarse n_fine/per_Coarse_grid maxIter spatialDimention maxCGiter solverType openmpThreads


To view the result,just run post.m in MATLAB.
