#include "CG_solver.h"
#include "utility.h"
#include <stdlib.h>
#include <cmath>
#include <x86intrin.h>
# include <xmmintrin.h>
# include <smmintrin.h>
#include <string.h>
#include <mpi.h>

#define BYTE_NUMBER 32
#define DATA_NUMBER 4

float op1[DATA_NUMBER] __attribute__((aligned(BYTE_NUMBER)))={0.f};
float op2[DATA_NUMBER] __attribute__((aligned(BYTE_NUMBER)))={0.f};
void spmxv(const float* val,const int *col,const int *row, const float *x,float *v,int nrows){	
	for (int i = 0; i < nrows; i++)
	{
		v[i] = 0.0f;
		for (int j = row[i];j<row[i+1];j++)
			v[i]+=val[j]*x[col[j]];
	}

//  5 point stencil , so one mul less than 8 || turn out to be slower!!!
   //      for (int i = 0 ;i < nrows; i++){
		 //    int numPoint  = row[i+1]-row[i];
 		// for (int j = 0 ; j<numPoint;j++) {
			// op1[j] = val[j+row[i]];
			// op2[j] = x[col[j+row[i]]];
		 // }
   //          v[i] = innerProd(op1,op2, DATA_NUMBER);
   //      }
}

void A_to_crs( float** A, float **A_fine,int chunkSize,int dofA, float **val,float **val_fine,int **col_ind,int **row_ptr){
	int nnz  = chunkSize*(chunkSize*3-2)+(chunkSize-1)*chunkSize*2;
	*val = (float*)aligned_alloc(BYTE_NUMBER,sizeof(float)*nnz);
	*val_fine = (float*)aligned_alloc(BYTE_NUMBER,sizeof(float)*nnz); 
	*col_ind = (int*)malloc(sizeof(int)*nnz);
	*row_ptr = (int*)malloc(sizeof(int)*(dofA+1));

	(*row_ptr)[0] = 0;
	int nzrow = 0;
    int cnt = 0;
	for (int i=0;i<dofA;i++){
		nzrow = 0;
		for (int j=0;j<dofA;j++)
		{
			if(fabs(A[i][j])>=0.00001f){
				(*val)[cnt] = A[i][j];
				(*val_fine)[cnt] = A_fine[i][j];
				(*col_ind)[cnt] = j;
				nzrow ++;
				cnt++;
			}
		}
		(*row_ptr)[i+1] = (*row_ptr)[i]+nzrow;
	}
	int myrank;
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank); // Get own ID
    if (0==myrank) printf("cnt=%d nnz=%d dofA((xum-2)^2=%d\n",cnt,nnz,dofA);
	if(cnt!=nnz ){
		printf("assembly error!\n");
		exit(-1);
	}

	if( 0!=(dofA%DATA_NUMBER)){
		printf("NOT aligned data!\n");
		exit(-1);
	}
}

void axpy(float *dest, float a, float *x, float *y, int n){//dest = a*x+y
    __m128 scalar = _mm256_set1_ps(a);
    __m128 loc_sum;
    __m128 loc_x;
    __m128 loc_y;

/*    for (int i= 0; i< n ; i+=DATA_NUMBER){
		loc_x = _mm256_load_ps(x+i);
		loc_y = _mm256_load_ps(y+i);
                loc_sum = _mm256_fmadd_ps(loc_x,scalar,loc_y);
     
                _mm256_store_ps(dest+i,loc_sum);
    }
*/
    for (int i= 0; i< n ; i+=DATA_NUMBER){
                loc_x = _mm_load_ps(x+i);
                loc_x = _mm_mul_ps(scalar,loc_x);
                loc_y = _mm_load_ps(y+i);
        loc_sum = _mm_add_ps(loc_x,loc_y);
        _mm_store_ps(dest+i,loc_sum);
    }

}


float innerProd(float *x, float *y, int n)
{
	const int mask = 0xFF;
	float product = 0.f;
	__m128 xSSE;
	__m128 ySSE;

	for (int i = 0 ; i < n ; i+=DATA_NUMBER){
		xSSE = _mm_load_ps(x+i);
		ySSE = _mm_load_ps(y+i);
		product += (_mm_dp_ps(xSSE,ySSE,mask))[0];
	}
	return product;
}

void vtrEqual(float a[],float b[],int n){
	memcpy(b,a,sizeof(float)*n);
}

int cg_solve(int dofA, float val[],int col_ind[],int row_ptr[],float b[],float epsilon,int maxit,float *x)
{
	int i,j,k;
	
	float* w = (float*)aligned_alloc(BYTE_NUMBER, sizeof(float) * dofA);
	float* p = (float*)aligned_alloc(BYTE_NUMBER, sizeof(float) * dofA);
	float* residual = (float*)aligned_alloc(BYTE_NUMBER, sizeof(float) * dofA);


	// init guess x0 = 1.0f,so r0 = b-A*x0 = b;
    for (int i = 0 ; i < dofA;i++) x[i] = 0.2f;
    spmxv(val,col_ind,row_ptr, x,residual,dofA);//w = A*p
    axpy(residual, -1.0f, residual, b, dofA);// residual=residual - alpha*w

    float rho = innerProd(residual,residual,dofA);
    float rhoSlash = 0.f;
    
	for (k = 0;k<maxit;k++){
		if(k == 0){
			vtrEqual(residual,p,dofA);
		}
		else{
			float beta = rho/rhoSlash;
			axpy(p, beta, p, residual, dofA);// p = beta*p+r
		}

		spmxv(val,col_ind,row_ptr, p,w,dofA);//w = A*p

		float alpha = rho/innerProd(p,w,dofA);

		axpy(x, alpha, p, x, dofA);// x+=alpha*p
		axpy(residual, -alpha, w, residual, dofA);// residual=residual - alpha*w
		rhoSlash = rho;
		rho = innerProd(residual,residual,dofA);
		// float beta = rho/rhoSlash;
		// axpy(p, beta, p, residual, dofA);// p = beta*p+r

		if(sqrt(rho)<=epsilon) 		break;

	}

	free(residual);
	free(p);
	free(w);
	return k;
}
