#include "GS_solver.h"
#include <iostream>
using namespace std;
static float lafResult[] = { 0.0f, 0.0f, 0.0f, 0.0f };

inline FloatIntrinsic DotV4( const FloatIntrinsic& lLhs, const FloatIntrinsic& lRhs )
{
    //M_CHECK_ALIGNMENT( lLhs, sizeof(FloatIntrinsic) );
    //M_CHECK_ALIGNMENT( lRhs, sizeof(FloatIntrinsic) );
    FloatIntrinsic lvMult = _mm_mul_ps( lLhs, lRhs );
    //_mm_storeu_ps(lafResult,lvMult);
    // printf( "%f, %f, %f, %f\n", lafResult[0], lafResult[1],lafResult[2], lafResult[3] );
    FloatIntrinsic lvTemp = _mm_shuffle_ps( lvMult, lvMult, _MM_SHUFFLE( 0,1,2,3 ) ); // W, Z, Y, X
    //    _mm_storeu_ps(lafResult,lvTemp);
    // printf( "%f, %f, %f, %f\n", lafResult[0], lafResult[1],lafResult[2], lafResult[3] );
    lvTemp = _mm_add_ps( lvTemp, lvMult ); // (x+w), (y+z), (z+y), (w+x)

     //       _mm_storeu_ps(lafResult,lvTemp);
     //printf( "%f, %f, %f, %f\n", lafResult[0], lafResult[1],lafResult[2], lafResult[3] );
    FloatIntrinsic lvTemp2 = _mm_shuffle_ps(lvTemp, lvTemp, _MM_SHUFFLE( 2, 3, 0, 1 ) );
    return _mm_add_ps( lvTemp, lvTemp2 ); // (x+w+z+y), (y+z+w+x), (z+y+x+w), (w+x+y+z)
}



// void run_gauss_seidel_method ( int n, float **A, float *b,float epsilon, int maxit,int *numit, float *x )
// {// Red-Black implementation for further parallelism
//    // [D 0;L D]-1*[0 U;0 0]*x_old + [D 0;L D]-1 * b = x_new 
//    // -R-B-R-B-...--R-B-
//    int i,j,k;
//    float *dx = new float[n];
//    FloatIntrinsic lvfA,lvfB; 
//    const int mask = 0xE1;//11110001
//    unsigned int redLast = (0==n%2)?(n-2):(n-1);
//    // printf("redLast= %d\n",redLast);
//    for(k=0; k<maxit; k++)
//    {
//       float sum = 0.0;
//       //red nodes update
//       for(i=1;i<redLast;i+=2){
//         dx[i] = b[i];
//         // for(j=i-1;j<i+2;j++)
//         // // for(j=0; j<n; j++)
//         //   dx[i] -= A[i][j]*x[j];
//         // self defined DotV4
//         lvfA = _mm_set_ps(A[i][i-1],A[i][i],A[i][i+1],0.0f);
//         lvfB = _mm_set_ps(x[i-1],x[i],x[i+1],0.0f);
//         // dx[i]-= (DotV4(lvfA,lvfB))[0];//slow
//         //
//         dx[i]-=(_mm_dp_ps(lvfA,lvfB,mask))[0];
        
//         dx[i] /= A[i][i]; 
//         x[i] += dx[i];
//         sum += ( (dx[i] >= 0.0) ? dx[i] : -dx[i]);
//       }
//       //black nodes update
//       for(i=2;i<n-1;i+=2){
//         dx[i] = b[i];
//         // for(j=i-1;j<i+2;j++)
//         // // for(j=0; j<n; j++)
//         //   dx[i] -= A[i][j]*x[j];

//                 // self defined DotV4
//         lvfA = _mm_set_ps(A[i][i-1],A[i][i],A[i][i+1],0.0f);
//         lvfB = _mm_set_ps(x[i-1],x[i],x[i+1],0.0f);
//         // dx[i]-= (DotV4(lvfA,lvfB))[0];//slow
//            dx[i]-=(_mm_dp_ps(lvfA,lvfB,mask))[0];
//         //
        
//         dx[i] /= A[i][i]; 
//         x[i] += dx[i];
//         sum += ( (dx[i] >= 0.0) ? dx[i] : -dx[i]);
//       }

//     //   for(i=1; i<n-1; i++)
//     //   {
//     //      dx[i] = b[i];
//     //      //for(j=0; j<n; j++)
//     //      for (j = i-1;j<i+2;j++)
//     //         dx[i] -= A[i][j]*x[j]; 
//     // //     lvfA = _mm_set_ps(A[i][i-1],A[i][i],A[i][i+1],0.0f);
//     // //     lvfB = _mm_set_ps(x[i-1],x[i],x[i+1],0.0f);
//     // //     dx[i]-= (DotV4(lvfA,lvfB))[0];//slow

//     // //     dx[i]-=(_mm_dp_ps(lvfA,lvfB,mask))[0];
//     //      dx[i] /= A[i][i]; 
//     //      x[i] += dx[i];
//     //      sum += ( (dx[i] >= 0.0) ? dx[i] : -dx[i]);
//     //   }

//       //printf("%4d : %.3e\n",k,sum);
//       if(sum <= epsilon) break;
//    }
//    *numit = k+1; 
//    delete [] dx;
// }

// void run_gauss_seidel_method ( int n, float **A, float *b,  float epsilon, int maxit,   int *numit, float *x )
// {
//   // n : dim of A = dofA
//    float *dx = new float[n];
//    int i,j,k;

//    for(k=0; k<maxit; k++)
//    {
//       float sum = 0.0;
//       for(i=0; i<n; i++)
//       {
//          dx[i] = b[i];
//          for(j=0; j<n; j++)
//             dx[i] -= A[i][j]*x[j]; 
//          dx[i] /= A[i][i];
//          x[i] += dx[i];
//          sum += ( (dx[i] >= 0.0) ? dx[i] : -dx[i]);
//       }
//       // printf("%4d : %.3e\n",k,sum);
//       if(sum <= epsilon) break;
//    }
//    *numit = k+1;
//    delete [] dx;
// }

// this is serial plain
/*void run_gauss_seidel_RB_method ( int n, int chunk, int redLen, unsigned int redIdx[], int blkLen, unsigned int blkIdx[], float **A, float *b,  float epsilon, int maxit,   int *numit, float *x )
{ // rb implmentation
  // n : dim of A = dofA
   
   int i,j,k,id;
   float *dx = new float[n];
   for(k=0; k<maxit; k++)
   {
      float sum = 0.0;
      //red nodes update
      for(i=0;i<redLen;i++){
        id = redIdx[i];
        dx[id] = b[id];
        // for(j=id-1;j<id+2;j++)
        for(j=0; j<n; j++)
          dx[id] -= A[id][j]*x[j];
        
        dx[id] /= A[id][id]; 
        x[id] += dx[id];
        sum += ( (dx[id] >= 0.0) ? dx[id] : -dx[id]);
      }
      //black nodes update
      for(i=0;i<blkLen;i++){
        id = blkIdx[i];
        dx[id] = b[id];
        // for(j=id-1;j<id+2;j++)
        for(j=0; j<n; j++)
          dx[id] -= A[id][j]*x[j];
        
        dx[id] /= A[id][id]; 
        x[id] += dx[id];
        sum += ( (dx[id] >= 0.0) ? dx[id] : -dx[id]);
      }
      // printf("%4d : %.3e\n",k,sum);
      if(sum <= epsilon) break;
   }
   *numit = k+1;
   delete [] dx;
}*/

// sse version 
void run_gauss_seidel_RB_method ( int n, int chunk, int redLen, unsigned int redIdx[], int blkLen, unsigned int blkIdx[], float **A, float *b,  float epsilon, int maxit,   int *numit, float *x )
{ // rb implmentation
  // n : dim of A = dofA
   
   int k,id;
   int dofA = chunk*chunk;
   int fstLineRed =   ((0==chunk%2)?(chunk-2):(chunk-1))/2+1;//in line zero
   int lstLineRed = fstLineRed ; // conditional when nx = ny
   int fstLineBlk = chunk - fstLineRed;
   int lstLineBlk = chunk - lstLineRed;
   // cout<<fstLineRed<<" "<<lstLineRed<<endl;
   // cout<<fstLineBlk<<" "<<lstLineBlk<<endl;

   FloatIntrinsic lvfA,lvfB; 
   // const int mask = 0xE1;//11110001
   const int mask = 0xFF;

   float *dx = (float*)malloc(sizeof(float)*n);
   
 
   for(k=0; k<maxit; k++)
   {
     float sum = 0.0;
     #pragma omp parallel reduction(+:sum) private(lvfA,lvfB,id)
      {
       // first line red
       #pragma omp for nowait 
      for(int i=0;i<fstLineRed;i++){
        id = redIdx[i];
        dx[id] = b[id];
        if (id!=0){
          lvfA = _mm_set_ps(0.0f,A[id][id-1],A[id][id+1],A[id][id+chunk]);
          lvfB = _mm_set_ps(0.0f,x[id-1],x[id+1],x[id+chunk]);
        }
        else{
          lvfA = _mm_set_ps(0.0f,0.0f,A[id][id+1],A[id][id+chunk]);
          lvfB = _mm_set_ps(0.0f,0.0f,x[id+1],x[id+chunk]);
        }
        dx[id]-=x[id]*A[id][id]+(_mm_dp_ps(lvfA,lvfB,mask))[0];
       
        dx[id] /= A[id][id]; 
        x[id] += dx[id];
        sum += ( (dx[id] >= 0.0) ? dx[id] : -dx[id]);
      } 
      //red nodes update
      #pragma omp for nowait 
      for(int i=fstLineRed;i<redLen-lstLineRed;i++){
        id = redIdx[i];
        dx[id] = b[id];
        lvfA = _mm_set_ps(A[id][id-chunk],A[id][id-1],A[id][id+1],A[id][id+chunk]);
        lvfB = _mm_set_ps(x[id-chunk],x[id-1],x[id+1],x[id+chunk]);
        dx[id]-=x[id]*A[id][id]+(_mm_dp_ps(lvfA,lvfB,mask))[0];
        
        // for(j=0; j<n; j++)
        //   dx[id] -= A[id][j]*x[j];

        dx[id] /= A[id][id]; 
        x[id] += dx[id];
        sum += ( (dx[id] >= 0.0) ? dx[id] : -dx[id]);
      }

      // last line red
      #pragma omp for nowait
       for(int i=redLen -lstLineRed;i<redLen;i++){
        id = redIdx[i];
        dx[id] = b[id];
        if (id!=dofA-1){
          lvfA = _mm_set_ps(A[id][id-chunk],A[id][id-1],A[id][id+1],0.0f);
          lvfB = _mm_set_ps(x[id-chunk],x[id-1],x[id+1],0.0f);
        }
        else{
          lvfA = _mm_set_ps(A[id][id-chunk],A[id][id-1],0.0f,0.0f);
          lvfB = _mm_set_ps(x[id-chunk],x[id-1],0.0f,0.0f);
        }
        dx[id]-=x[id]*A[id][id]+(_mm_dp_ps(lvfA,lvfB,mask))[0];
        
        dx[id] /= A[id][id]; 
        x[id] += dx[id];
        sum += ( (dx[id] >= 0.0) ? dx[id] : -dx[id]);
      } 
    }
    
    #pragma omp parallel reduction(+:sum)
    {
    // first line blk
     #pragma omp for nowait
      for(int i=0;i<fstLineBlk;i++){
        id = blkIdx[i];
        dx[id] = b[id];

        lvfA = _mm_set_ps(0.0f,A[id][id-1],A[id][id+1],A[id][id+chunk]);
        lvfB = _mm_set_ps(0.0f,x[id-1],x[id+1],x[id+chunk]);
        
        dx[id]-=x[id]*A[id][id]+(_mm_dp_ps(lvfA,lvfB,mask))[0];
        
        dx[id] /= A[id][id]; 
        x[id] += dx[id];
        sum += ( (dx[id] >= 0.0) ? dx[id] : -dx[id]);
      } 
    
      //black nodes update
      #pragma omp for nowait
      for(int i=fstLineBlk;i<blkLen-lstLineBlk;i++){
        id = blkIdx[i];
        dx[id] = b[id];
        lvfA = _mm_set_ps(A[id][id-chunk],A[id][id-1],A[id][id+1],A[id][id+chunk]);
        lvfB = _mm_set_ps(x[id-chunk],x[id-1],x[id+1],x[id+chunk]);
        dx[id]-=x[id]*A[id][id]+(_mm_dp_ps(lvfA,lvfB,mask))[0];

        // for(j=0; j<n; j++)
        //   dx[id] -= A[id][j]*x[j];

        dx[id] /= A[id][id]; 
        x[id] += dx[id];
        sum += ( (dx[id] >= 0.0) ? dx[id] : -dx[id]);
      }

      // last line blk
      #pragma omp for nowait
       for(int i=blkLen -lstLineBlk;i<blkLen;i++){
        id = blkIdx[i];
        dx[id] = b[id];
        if (id!=dofA-1){
          lvfA = _mm_set_ps(A[id][id-chunk],A[id][id-1],A[id][id+1],0.0f);
          lvfB = _mm_set_ps(x[id-chunk],x[id-1],x[id+1],0.0f);
        }
        else{
          lvfA = _mm_set_ps(A[id][id-chunk],A[id][id-1],0.0f,0.0f);
          lvfB = _mm_set_ps(x[id-chunk],x[id-1],0.0f,0.0f);
        }
        dx[id]-=x[id]*A[id][id]+(_mm_dp_ps(lvfA,lvfB,mask))[0];

        dx[id] /= A[id][id]; 
        x[id] += dx[id];
        sum += ( (dx[id] >= 0.0) ? dx[id] : -dx[id]);
      } 
    }

      // printf("%4d : %.3e\n",k,sum);
      if(sum <= epsilon) break;
   }
   *numit = k+1;
  free(dx);
}