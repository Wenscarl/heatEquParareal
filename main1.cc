# include <cstdlib>
# include <iostream>
# include <iomanip>
# include <fstream>
# include <ctime>
# include <cmath>
# include "utility.h"
# include "r83_cr_solver.h"
# include "GS_solver.h"
using namespace std;
int MaxIter ;
int main (int argc,char *argv[] )
//
// All changes to code are copyright, 2017, shu wang, shuwang12@unm.edu
//  Purpose:
//
//   This is the main program for solving heat equation by parareal algorithm.
//
//  Note:
//
//    Parareal solves the heat equation with an implicit method, backward euler.
//
//    This program solves
//
//      dUdT - k * d2UdX2 = F(X,T)
//
//    over the interval [A,B] with boundary conditions
//
//      U(A,T) = UA(T),
//      U(B,T) = UB(T),
//
//    over the time interval [T0,T1] with initial conditions
//
//      U(X,T0) = U0(X)
//
//
//  Licensing: 
//  UNM CS 542 
//

//
//  Modified:
//
//    07 Feb 2017
//
//  Author:
//
//    Shu Wang
//
{
  float **a,**a_fine;
  bool header;
  int i;
  int j;
  int n_Coarse,n_FineTot,n_Fine;
  int IterK ;
  float k;
  float *t_Coarse,*t_Fine;
  float t_dtFine;
  float t_dtCoarse;
  string t_file;
  float t_max;
  float t_min;
  float *u_C,*u_F,*uC_corr;
  string u_file;
  float w_coarse,w_fine;
  float *x,*y;
  float x_delt;
  string x_file;
  float x_max;
  float x_min;
  int x_num;

  if(argc >=6){
    n_Coarse = atoi(argv[1]);
    n_Fine = atoi(argv[2]);
    IterK = atoi(argv[3]);
    x_num = atoi(argv[4]);
    MaxIter = atoi(argv[5]);
  }
  else{//default
    n_Coarse = 5;
    n_Fine = 4;
    IterK =  5;
    x_num = 45;
    MaxIter = 700;
  }
  n_FineTot = n_Coarse*n_Fine;

  timestamp ( );
  cout << "\n";
  cout << "FD2D_HEAT_IMPLICIT_BY_ParaReal\n";
  cout << "  C++ version\n";
  cout << "\n";
  cout << "  Finite difference solution of\n";
  cout << "  the time dependent 1D heat equation\n";
  cout << "\n";
  cout << "    Ut - k * Uxx = F(x,t)\n";
  cout << "\n";
  cout << "  for space interval A <= X <= B with boundary conditions\n";
  cout << "\n";
  cout << "    U(A,t) = UA(t)\n";
  cout << "    U(B,t) = UB(t)\n";
  cout << "\n";
  cout << "  and time interval T0 <= T <= T1 with initial condition\n";
  cout << "\n";
  cout << "    U(X,T0) = U0(X).\n";
  cout << "\n";
  cout << "  A second order difference approximation is used for Uxx.\n";
  cout << "\n";
  cout << "  A first order backward Euler difference approximation\n";
  cout << "  is used for Ut.\n";
  cout << " Gauss-Seidel iterative solver is used!\n";
  cout <<  n_Coarse<<" Coarse Grids\n";
  cout <<  n_FineTot<<" Fine Grids \n";
  cout <<  IterK<<" Iterations\n";
  cout << "  is used for Ut.\n";

  k = 5.0E-07;
//
//  Set X values.
//
  x_min = 0.0;
  x_max = 0.3;
//   x_num = 11;
  x_delt = ( x_max - x_min ) / ( float ) ( x_num - 1 );

  x = new float[x_num];
  y = new float[x_num];

  for ( i = 0; i < x_num; i++ )
  {
    x[i] = ( ( float ) ( x_num - i - 1 ) * x_min  + ( float ) (i) * x_max )  / ( float ) ( x_num - 1 );
    y[i] = ( ( float ) ( x_num - i - 1 ) * x_min  + ( float ) (i) * x_max )  / ( float ) ( x_num - 1 );
  }
// 
//  Set T values.
//
  t_min = 0.0;
  t_max = 1000.0;
  

  t_dtCoarse = ( t_max - t_min ) / ( float ) ( n_Coarse );
  t_dtFine = ( t_max - t_min ) / ( float ) ( n_FineTot );

  t_Coarse = new float[n_Coarse+1];
  t_Fine = new float[n_FineTot+1];

    for ( j = 0; j < n_Coarse+1; j++ ){
    t_Coarse[j] = ( ( float ) ( n_Coarse+1 - j - 1 ) * t_min   
           + ( float ) (         j     ) * t_max ) 
           / ( float ) ( n_Coarse+1     - 1 );
  }
    for ( j = 0; j < n_FineTot+1; j++ ){
    t_Fine[j] = ( ( float ) ( n_FineTot+1 - j - 1 ) * t_min   
           + ( float ) (         j     ) * t_max ) 
           / ( float ) ( n_FineTot+1     - 1 );
  }
  int *pick = new int[n_Coarse+1];
  for (j=0;j<n_Coarse+1;j++){
    pick[j] = j*(n_Fine+1);
  }

//
//  The matrix A does not change with time.  We can set it once,
//  factor it once, and solve repeatedly.
//
  int chunkSize = x_num-2;
  int dofA = chunkSize*chunkSize;

  w_coarse = k * t_dtCoarse / x_delt / x_delt;
  a = new float*[dofA];
  for (int i = 0;i < dofA;i++){
    a[i] = new float[dofA];
    memset(a[i],0.0,dofA*sizeof(float));
  }

 // set matrix for coarse grid
  setMatrix(a,chunkSize,w_coarse);
   // printMat(dofA,a);

  w_fine = k * t_dtFine / x_delt / x_delt;
  a_fine = new float*[dofA];
  for (int i = 0;i < dofA;i++){
    a_fine[i] = new float[dofA];
    memset(a_fine[i],0.0,dofA*sizeof(float));
  }
  setMatrix(a_fine,chunkSize,w_fine);
  // printMat(dofA,a_fine);
  // setup red-black ordering
  unsigned int *redIdx,*blkIdx;
  int redLen,blkLen;

  RBordering( chunkSize, dofA,&redIdx,&redLen,&blkIdx,&blkLen);


  // Start-up serial;  Set the initial data, for time T_MIN.
  u_C = new float[dofA*(n_Coarse+1)];
  u_F = new float[dofA*(n_Coarse*(n_Fine+1))];
  uC_corr = new float[dofA*(n_Coarse*2)];

  // setting up initial condition 2d
  // u0 ( x_min, x_max, t_min, x_num, x, u_F);
  // u0 ( x_min, x_max, t_min, x_num, x, u_C);
  // u0 ( x_min, x_max, t_min, x_num, x, uC_corr);

  // setting up initial condition 2d, NOTICE: initialize first data of u
  u0_2d(x_min, x_max, t_min, chunkSize, x, y, u_F);
  u0_2d(x_min, x_max, t_min, chunkSize, x, y, u_C);
  u0_2d(x_min, x_max, t_min, chunkSize, x, y, uC_corr);

   //heat2dBe_GS(n_Coarse+1,x_num,a_cr_coarse,u_C);
  //

  // Parareal Loop
  for (int k = 0;k < IterK; k++){
      cout<<"Iteration "<<k<<endl;
      MapC2F(n_Coarse, dofA, u_C,u_F,pick);
      if(0 == k){
        for(j = 0;j<n_Coarse;j++){
          float *sliceBegin;

          sliceBegin = u_F+dofA*pick[j];
          // printVtr(dofA,sliceBegin);
          //heat2dBe(n_Fine+1,x_num,a_cr_fine,sliceBegin);
          heat2dBe_GS(a_fine,redLen,redIdx,blkLen, blkIdx,n_Fine+1,dofA,chunkSize,sliceBegin);
        }
      }
      else{
          for(j = k;j<n_Coarse;j++){
          float *sliceBegin;
          sliceBegin = u_F+j*dofA*(n_Fine+1);
          //heat2dBe(n_Fine+1,x_num,a_cr_fine,sliceBegin);
          heat2dBe_GS(a_fine,redLen,redIdx,blkLen, blkIdx,n_Fine+1,dofA,chunkSize,sliceBegin);
          // if(j==n_Coarse-1) printVtr(dofA,sliceBegin);
        } 
      }
      // correction-parallel
      MapC2corr(n_Coarse,dofA, u_C, uC_corr);
      for (j=k;j<n_Coarse;j++){
        float *sliceBegin;
        sliceBegin = uC_corr+j*dofA*(1+1);
        //heat2dBe(2,x_num,a_cr_coarse,sliceBegin);
        heat2dBe_GS(a, redLen,redIdx,blkLen,blkIdx,2,dofA,chunkSize,sliceBegin);
      }
      // update-serial
      for (j=k;j<n_Coarse;j++){
          float *sliceBegin;
          sliceBegin = u_C+j*dofA;
          //heat2dBe(2,x_num,a_cr_coarse,sliceBegin);
          heat2dBe_GS(a, redLen,redIdx,blkLen,blkIdx,2,dofA,chunkSize,sliceBegin);
          float *pC,*pF,*p_corr;
          pC = u_C+(j+1)*dofA;
          pF = u_F+(pick[j+1]-1)*dofA;
          p_corr = uC_corr+(j*2+1)*dofA;
          UpdateCoarse(dofA,pC,pF,p_corr);
      }
  }

  x_file = "x.txt";
  header = false;
  dtable_write ( x_file, 1, x_num, x, header );

  cout << "\n";
  cout << "  X data written to \"" << x_file << "\".\n";

  t_file = "t_c.txt";
  header = false;
  dtable_write ( t_file, 1, n_Coarse+1, t_Coarse, header );
  cout << "  T coarse data written to \"" << t_file << "\".\n";
  
  t_file = "t_f.txt";
  dtable_write ( t_file, 1, n_FineTot+1, t_Fine, header );
  cout << "  T fine data written to \"" << t_file << "\".\n";

  u_file = "u_c.txt";
  header = false;
  dtable_write ( u_file, dofA, n_Coarse+1, u_C, header);
  cout << "  U coarse data written to \"" << u_file << "\".\n";
  
  u_file = "u_f.txt";
  header = false;
  dtable_write ( u_file, dofA, n_Coarse*(n_Fine+1), u_F, header ,true,n_Fine);
  cout << "  U fine data written to \"" << u_file << "\".\n";
  
  cout << "\n";
  cout << "FD2D_HEAT_IMPLICIT\n";
  cout << "  Normal end of execution.\n";
  cout << "\n";
  timestamp ( );


// free memory
  for (int i = 0;i < dofA; i++ )  {delete [] a[i];delete a_fine[i];}

  delete [] a;
  delete [] a_fine;
  delete [] t_Coarse;
  delete [] t_Fine;
  delete [] pick;
  delete [] u_C;
  delete [] u_F;
  delete [] uC_corr;
  delete [] x;
  delete [] y;
  delete [] redIdx;
  delete [] blkIdx;
  
  return 0;
}

